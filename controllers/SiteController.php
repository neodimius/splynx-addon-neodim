<?php

namespace app\controllers;

use Yii;
use app\models\Customer;
use app\models\InternetServices;
use app\models\VoiceServices;
use app\models\CustomServices;

use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\web\Request;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
            $model = new Customer();

            $pagination = new Pagination ([
                'defaultPageSize' => 3, // Put count of Customers for rendering
                'totalCount' => $model->getCountOfCustomers(),
            ]);
            
            $customers = $model->getCustomers($pagination->offset, $pagination->limit);

            return $this->render('index', [
                'customers' => $customers,
                'pagination' => $pagination,
            ]);
    }
    
    public function actionServices()
    {
        $id = Yii::$app->request->get('customer_id');
        $customer = Yii::$app->request->get('customer_name');

        if ($id && $customer){
            return $this->render('services', [
                'internet' => new InternetServices(),
                'voice' => new VoiceServices(),
                'custom' => new CustomServices(),
                'id' => $id,
                'customerName' => $customer,
            ]);

        }
    }
}
