<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'Splynx Add-on Neodim New';
    }

    public function getModuleName()
    {
        return 'splynx_addon_neodim';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'], ['services'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
        ];
    }

    public function getEntryPoints()
    {
        return [
            [
                'name' => 'neodim_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Fneodim',
                'icon' => 'fa-user',
            ],
        ];
    }
}
