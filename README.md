My First Splynx Add-on
======================

Splynx Add-on Neodim based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.4"
composer install
~~~

Install Splynx Add-on Neodim:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/neodimius/splynx-addon-neodim.git
cd splynx-addon-neodim
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-neodim/web/ /var/www/splynx/web/neodim
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-neodim.addons
~~~

with following content:
~~~
location /neodim
{
        try_files $uri $uri/ /neodim/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Add-On Neodim in menu "Customers".