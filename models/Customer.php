<?php

namespace app\models;

use splynx\models\customer\BaseCustomer;

class Customer extends BaseCustomer
{
    public function getCountOfCustomers()
    {
        $count = count($this->findAll(
            [],
            [],
            [
                'id' => 'ASC'
            ]
        ));

        return $count;
    }

    public function getCustomers($offset, $limit)
    {
        $models = $this->findAll([
            'id' => ['BETWEEN', $offset, $offset+$limit]
        ]);

        return $models;
    }
















}
