<?php

namespace app\models;

use splynx\models\services\VoiceService;

class VoiceServices extends VoiceService
{
    public function getVoiceServices($customer_id)
    {
        $models = $this->findByCustomerId($customer_id);

        return $models;
    }
}
