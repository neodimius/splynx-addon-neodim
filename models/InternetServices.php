<?php

namespace app\models;

use splynx\models\services\InternetService;

class InternetServices extends InternetService
{
    public function getInternetServices($customer_id)
    {
        $models = $this->findByCustomerId($customer_id);

        return $models;
    }
}
