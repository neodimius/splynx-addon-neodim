<?php

namespace app\models;

use splynx\models\services\CustomService;

class CustomServices extends CustomService
{
    public function getCustomServices($customer_id)
    {
        $models = $this->findByCustomerId($customer_id);

        return $models;
    }
}
